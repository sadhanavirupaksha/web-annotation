injectJquery();

function injectJquery() {
    var body = document.getElementsByTagName('body')[0];
    var scripttag = document.createElement('script');
    scripttag.type = "text/javascript";
    scripttag.src = "https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"; 
    body.appendChild(scripttag);
    getJsondata();
}

function getJsondata()
{
	disableAllLinks() // disables all the links in a page
    var url = "https://gl.githack.com/renarration/web-annotation/raw/master/src/annolet-bookmarklet/annolet/css/annolet.css"; 
    $.get(url, function(data) {
        console.log("success");
        json_data = data;
    })
        .done(function() {
            annoletContainer();
        })
        .fail(function(data, textStatus, xhr) {
            alert( data.status+""+xhr+"error");
            console.log("error", data.status); //This shows status code eg. 403
            console.log("STATUS:"+xhr);   //This shows status message eg. Forbidden
        })
        .always(function() {
            console.log("ended");   //TO-DO after fail/done request.
        });
}

// function to create container for annolet
function annoletContainer(){
    //appending a div(annolet container) to body element of a webpage.
    var body = document.getElementsByTagName('body')[0];
    container = document.createElement('div');
    container.id = 'annolet-container';
    body.appendChild(container);

    //appending a CSS stylesheet into head element of a webpage, which is used to stylize the annolet container.
    var linktag = document.createElement('link');
    linktag.rel = "stylesheet";
    linktag.type = "text/css";
    linktag.href = json_data.cssfiles["annolet"]; 
    document.getElementsByTagName('head')[0].appendChild(linktag);

    //injecting html code
    container.innerHTML = "<h4 id='annolet-header'>Renarration</h4>"+
    "<ul id='annolet-tools-menu' >"+
        "<li class='annolet-menu-item' style='position:relative;top:-25px'>"+
            "<button id='disable-css' class='annolet-menu-sub-item' title='Click on the button to view the page without css'>No CSS</button>"+
        "</li>"+
        "<li class='annolet-menu-item' style='position:relative;top:-25px' title='Click on the button first and then point out the elements to remove the clutter'>"+
            "<button id='zapper' class='annolet-menu-sub-item' >Zapper</button>"+
        "</li>"+
        "<li class='annolet-menu-item' style='position:relative;top:-25px' title='Click on the button first and then modify the content on a page' >"+
            "<button id='modify-content' class='annolet-menu-sub-item' >Modify Content</button>"+
        "</li>"+
        "<li class='annolet-menu-item' style='position:relative;top:-25px' title='Select the text first and then click on the button to highlight the text' >"+
            "<button id='highlighter-btn' class='annolet-menu-sub-item' >Highlighter</button>"+
        "</li>"+
        "<li class='annolet-menu-item' style='position:relative;top:-25px' title='Select the word to get translated and then click on the button'>"+
            "<button id='phonetics-btn' class='annolet-menu-sub-item' >Phonetics</button>"+
        "</li>"+
        "<li class='annolet-menu-item' title='Select the choice from the dropdown, select the text and then click on the button to get the translation for the selected text'>"+
            "<button id='trans-text' class='annolet-menu-sub-item' >Translate Text</button>"+"<br>"+
            "<select class='select-tools-menu' id='select-from-lang'>"+
                "<option value='en' >English</option>"+
                "<option value='hi' >Hindi</option>"+
                "<option value='te' >Telugu</option>"+
                "<option value='ta' >Tamil</option>"+
                "<option value='ml' >Malayalam</option>"+
                "<option value='ja' >Japanese</option>"+
                "<option value='zh' >Chinese</option>"+
            "</select>"+
            "<select class='select-tools-menu' id='select-to-lang'>"+
                "<option value='zh' >Chinese</option>"+
                "<option value='ja' >Japanese</option>"+
                "<option value='ml' >Malayalam</option>"+
                "<option value='ta' >Tamil</option>"+
                "<option value='te' >Telugu</option>"+
                "<option value='hi' >Hindi</option>"+
                "<option value='en' >English</option>"+
            "</select>"+
        "</li>"+
        "<li class='annolet-menu-item' title='Select the theme and then click on button'>"+
            "<button id='change-theme' class='annolet-menu-sub-item'>Switch CSS</button>"+"<br>"+
            "<select class='select-tools-menu' id='select-theme'>"+
                "<option value='switch1' >Theme1</option>"+
                "<option value='switch2' >Theme2</option>"+
                "<option value='switch3' >Theme3</option>"+
            "</select>"+
        "</li>"+
        "<li class='annolet-menu-item' title='Click on the button to view the links/images/text on a page by selecting the option from the dropdown'>"+
            "<button id='change-content' class='annolet-menu-sub-item'>Page Stripper</button>"+"<br>"+
            "<select class='select-tools-menu' id='select-content'>"+
                "<option value='show-text' >Show Text</option>"+
                "<option value='show-links' >Show Links</option>"+
                "<option value='show-images' >Show Images</option>"+
            "</select>"+
        "</li>"+
        "<li class='annolet-menu-item' title='Select the choice of visibility from dropdown and then click on the button to increase/decrease the font of a page'>"+
            "<button id='change-font' class='annolet-menu-sub-item' >Visibility</button>"+"<br>"+
            "<select class='select-tools-menu' id='select-font'>"+
                "<option value='increase-font' >Increase Font</option>"+
                "<option value='decrease-font' >Decrease Font</option>"+
            "</select>"+
        "</li>"+
        "<li class='annolet-menu-item' title='Select the choices from the dropdown, select the currency on page and then click on the button to translate the selected currency'>"+
            "<button id='change-currency' class='annolet-menu-sub-item' >Convert Currency</button>"+"<br>"+
            "<select class='select-tools-menu' id='select-from-currency'>"+
                "<option value='USD' >USD</option>"+
                "<option value='INR' >INR</option>"+
                "<option value='EUR' >EUR</option>"+
            "</select>"+
            "<select class='select-tools-menu' id='select-to-currency'>"+
                "<option value='INR' >INR</option>"+
                "<option value='USD' >USD</option>"+
                "<option value='EUR' >EUR</option>"+
            "</select>"+
        "</li>"+
        "<li class='annolet-menu-item' title='Select the choices from the dropdown, select the units on page and then click on the button to translate the selected unit'>"+
            "<button id='change-measurement' class='annolet-menu-sub-item' >Convert Measurements</button>"+"<br>"+
            "<select class='select-tools-menu' id='select-from-measure'>"+
                "<option value='km'>kilometers</option>"+
                "<option value='mi'>Miles</option>"+
                "<option value='ft'>foot</option>"+
            "</select>"+
            "<select class='select-tools-menu' id='select-to-measure'>"+
                "<option value='mi'>Miles</option>"+
                "<option value='km'>kilometers</option>"+
                "<option value='ft'>foot</option>"+
            "</select>"+
        "</li>"+
        "<li class='annolet-menu-item' title='Select the choice of format from the dropdown, select the num on page and then click on a button to format the selected num'>"+
            "<button id='change-num-sys' class='annolet-menu-sub-item' >Convert Num sys</button>"+"<br>"+
            "<select class='select-tools-menu' id='select-num-sys'>"+
                "<option value='en-IN' >Indian</option>"+
                "<option value='en-US' >US</option>"+
                "<option value='en-GB'>British</option>"+
                "<option value='ko-KR'>Korean</option>"+
                "<option value='ar-EG'>Arabic</option>"+
            "</select>"+
        "</li>"+
        "<li class='annolet-menu-item' title='Select the choice of format from the dropdown, select the date on page and then click on a button to format the selected date'>"+
            "<button id='change-date-format' class='annolet-menu-sub-item' >Date Format</button>"+"<br>"+
            "<select class='select-tools-menu' id='select-date-format'>"+
                "<option value='en-IN' >Indian</option>"+
                "<option value='en-US' >US</option>"+
                "<option value='en-GB'>British</option>"+
                "<option value='ko-KR'>Korean</option>"+
                "<option value='ar-EG'>Arabic</option>"+
            "</select>"+
        "</li>"+
    "</ul>";
    setTimeout(function() {
    	alert("Page is ready to Renarrate");
    }, 1000);

    addClickevents();
}

// function to disable all links
function disableAllLinks(){
    var anchors = document.getElementsByTagName("a");
    for (var i = 0; i < anchors.length; i++) {
        anchors[i].onclick = function() {return(false);};
    }
}

// function to remove the css
function disableCss(){
    var styleSheets = document.styleSheets;
    for ( var i=0; i<styleSheets.length; i++) {
        if(styleSheets[i].href == json_data.cssfiles["annolet"] ){
           styleSheets[i].disabled = false;
        }
        else{
            styleSheets[i].disabled = true;
        }
    }
}

//function to erase the content 
function  ZapPer(){
    $("body").click(function(event){
        var targetElem= event.target;
        if(targetElem.id == "annolet-container"||targetElem.id =="zapper"||targetElem.id =="annolet-header")
        {           
            targetElem.style.visibility = "visible";
        }
        else
        {
            targetElem.style.visibility = "hidden";
        }
    });
}

function modifyContent() {
    var all = document.getElementsByTagName("*");
    for (var i=0,  max=all.length; i < max; i++) { 
        all[i].setAttribute('contenteditable', true);
        all[i].setAttribute('title', 'Edit Content');
    }
}

function getSelectedText(){ 
    if(window.getSelection){
        return window.getSelection().toString(); 
    } 
    else if(document.getSelection){
        return document.getSelection(); 
    } 
    else if(document.selection){
        return document.selection.createRange().text; 
    }
    return ''; 
}

function annoHighlight() {
	var current = getSelectedText();
	var userSelection_firefox = window.getSelection();
	if(current != "") {
		var span_elem = document.createElement('span');
        span_elem.textContent = current;
        if(window.getSelection){
        	highlightContent(span_elem, userSelection_firefox);
        }
        else if(document.getSelection || document.selection){
        	highlightContent(span_elem, current); 
    	} 
	}
	else if(current == "") {
		alert("Select text and click on highlighter button");
	}
}

function highlightContent(elem, str) {      
     elem.style.backgroundColor = "yellow";
     elem.id = 'highlighted';
     var range = str.getRangeAt(0);
     range.deleteContents();
     range.insertNode(elem);
}

function phoneticsTrans(){
	var current = getSelectedText();
    var url = "//localhost:5000/phonetic-trans"
    if(current != "") {
	    var xhr = new XMLHttpRequest();
	    xhr.open("POST",url,true);
	    xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
	    xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
	    xhr.send(JSON.stringify({"sentence": current}));
	    xhr.onreadystatechange = function() {
	        if (this.readyState==4 && this.status==200) {
	        	var res = this.responseText;
			    var span_elem = document.createElement('span');
        		span_elem.textContent = res;
        		span_elem.style.backgroundColor = "yellow";
     			span_elem.id = 'highlighted';
     			var range = window.getSelection().getRangeAt(0);
			    range.deleteContents();
			    range.insertNode(span_elem);
	        }
	    }
	}
	else if(current == "") {
		alert("Select the word and click on 'Phonetics' button");
	}
}

function langTrans(){
	var from_lang = document.getElementById("select-from-lang").value;
   	var to_lang = document.getElementById("select-to-lang").value;
   	var current = getSelectedText();
   	if(current != "") { 
   		var url = "//localhost:5000/language-translive";
   		var xhr = new XMLHttpRequest();
	    xhr.open("POST",url,true);
	    xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
	    xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
	    xhr.send(JSON.stringify( {"sentence":current, "from-language":from_lang, "to-language":to_lang} ));
	    xhr.onreadystatechange = function() {
	        if (this.readyState==4 && this.status==200) {
	        	var res = this.responseText;
			    var span_elem = document.createElement('span');
        		span_elem.textContent = res;
        		span_elem.style.backgroundColor = "yellow";
     			span_elem.id = 'highlighted';
     			var range = window.getSelection().getRangeAt(0);
			    range.deleteContents();
			    range.insertNode(span_elem);
	        }
	    }
    }
    else if(current == "") {
		alert("Select the text and click on 'Translate Text' button");
	}
}

function alternateStylesheets(){
    //appending a CSS alternate stylesheets to head element of a webpage.
    var i= 0;
    var style_sheets = 3; 
    var css_themes =[ json_data.cssfiles["Theme1"], json_data.cssfiles["Theme2"], json_data.cssfiles["Theme3"] ];
    var link_title =['switch1', 'switch2', 'switch3'];

    for(i=0; i<style_sheets; i++){
        var linktag = document.createElement('link');
        linktag.rel  = 'alternate stylesheet';
        linktag.type = 'text/css';
        linktag.href = css_themes[i];
        linktag.title= link_title[i];
        head  = document.getElementsByTagName('head')[0];
        head.appendChild(linktag);
    }
    var selected_theme = document.getElementById("select-theme").value;
    switchStyle(selected_theme)
}

function switchStyle(css_title)
{   
   var i;
   var linktag = document.getElementsByTagName("link");
   for (i = 0; i < linktag.length; i++ ) {
        if ((linktag[i].rel.indexOf( "stylesheet" ) != -1) &&linktag[i].title) {
            linktag[i].disabled = true ;
            if (linktag[i].title == css_title) {
                linktag[i].disabled = false ;
            }
        }
   }
}

function showContent(){
    var all = document.getElementsByTagName("*");
    var selected_content = document.getElementById("select-content").value;
    if(selected_content == 'show-links'){
        for (var i=0, max=all.length; i < max; i++) {
            var href_attribute = all[i].hasAttribute("href");
            var src_attribute = all[i].hasAttribute("src");
            if(href_attribute == true || src_attribute == true){
                all[i].style.visibility = 'visible';
            }
            else if(href_attribute == false && src_attribute == false){
                all[i].style.visibility = 'hidden';
            }
        }
    }
    else if(selected_content == 'show-text') {
        for (var i=0, max=all.length; i < max; i++) {
            if(all[i].innerHTML){
                all[i].style.visibility = 'visible';
            }
            else{
                all[i].style.visibility = 'hidden';
            }
        }
    }
    else if(selected_content == 'show-images') {
        for (var i=0, max=all.length; i < max; i++) {
            var src_attribute = all[i].hasAttribute("src");
            if(src_attribute == false){
                all[i].style.visibility = 'hidden';
            }
            else if(src_attribute == true){
                all[i].style.visibility = 'visible';
            }
        }
    }

    //get the menu bar id 
    document.getElementById('annolet-container').style.visibility='visible';
    var annolet_elems = document.getElementById('annolet-container').children;
    //This will make all children elements of div visible. 
    for(var i = 0; i < annolet_elems.length; i++){
        annolet_elems[i].style.visibility = 'visible';
    }
}

function changeFont(){
    var fontSize = parseInt($('body').css('font-size'),10);
    var selected_font = document.getElementById("select-font").value;
    if(selected_font == 'increase-font'){
        fontSize +=1.5;
        $('body').css('font-size', fontSize+'px');
    }
    else if(selected_font == 'decrease-font'){
        fontSize -=1;
        $('body').css('font-size', fontSize+'px');
    }
}

function currencyTrans(){
	var from_cur = document.getElementById('select-from-currency').value;
    var to_cur = document.getElementById('select-to-currency').value;
    var current = getSelectedText();
    var remove_splchar = current.replace(/\,/g,"");
    var currency = parseFloat(remove_splchar);
    console.log(currency);
    if(current != "") {
    	if(currency !== NaN) {
	    	var url = "//localhost:5000/currency-conversion";
	   		var xhr = new XMLHttpRequest();
		    xhr.open("POST", url, true);
		    xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
		    xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
		    xhr.send(JSON.stringify( { "currency_amount":currency, "from_cur":from_cur, "to_cur":to_cur } ));
		    xhr.onreadystatechange = function() {
		        if (this.readyState==4 && this.status==200) {
		        	var res = this.responseText;
				    var span_elem = document.createElement('span');
	        		span_elem.textContent = res;
	        		span_elem.style.backgroundColor = "yellow";
	     			span_elem.id = 'highlighted';
	     			var range = window.getSelection().getRangeAt(0);
				    range.deleteContents();
				    range.insertNode(span_elem);
		        }
		    }
		}
		else if( currency === NaN ){
			alert("select the currency");
		}
    }
    else if(current == "") {
    	alert("Select the currency and click on 'Convert Currency' button");
    }
}

//function for getting units of measurement conversion
function convertUnits(){
	var from_unit = document.getElementById('select-from-measure').value;
    var to_unit = document.getElementById('select-to-measure').value;
    var current = getSelectedText();
    var remove_splchar = current.replace(/\,/g,"");
    var units = parseFloat(remove_splchar);
    console.log(units);
    if(current != "") {
    	if(units !== NaN) {
	    	var url = "//localhost:5000/measurement-conversion";
	   		var xhr = new XMLHttpRequest();
		    xhr.open("POST", url, true);
		    xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
		    xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
		    xhr.send(JSON.stringify({ "measurement_num": units, "from_measure": from_unit, "to_measure": to_unit }));
		    xhr.onreadystatechange = function() {
		        if (this.readyState==4 && this.status==200) {
		        	var res = this.responseText;
				    var span_elem = document.createElement('span');
	        		span_elem.textContent = res;
	        		span_elem.style.backgroundColor = "yellow";
	     			span_elem.id = 'highlighted';
	     			var range = window.getSelection().getRangeAt(0);
				    range.deleteContents();
				    range.insertNode(span_elem);
		        }
		    }
		}
		else if( units === NaN ){
			alert("select the units");
		}
    }
    else if(current == "") {
    	alert("Select the units and click on 'Convert Measurements' button");
    }
}

function convertNumsys(){
    var selected_numsys = document.getElementById("select-num-sys").value;
    if (window.getSelection) 
    {
        var number = window.getSelection().toString();
        var remove_splchar =  number.replace(/\,/g,"");
        var num_to_translate = parseInt(remove_splchar);
    } 
    else if (document.selection && document.selection.type != "Control") {
        var number = document.selection.createRange().text;
        var remove_splchar =  number.replace(/\,/g,"");
        var num_to_translate = parseInt(remove_splchar);
    }
    var translated_num = num_to_translate.toLocaleString(selected_numsys);
    var parent = $(window.getSelection().focusNode.parentElement);
    var oldHtml = parent.html();
    var newHtml = oldHtml.replace(number, "<span class='highlight' style='color:green'>"+translated_num+"</span>");
    parent.html( newHtml );
}

function formatDate(){
    var selected_format = document.getElementById("select-date-format").value;
    if (window.getSelection) 
    {
        var date_str = window.getSelection().toString();
        var date_to_format = new Date(date_str);
    } 
    else if (document.selection && document.selection.type != "Control") {
        var date_str = document.selection.createRange().text;
        var date_to_format = new Date(date_str);
    }
    var formated_date = date_to_format.toLocaleDateString(selected_format);
    var parent = $(window.getSelection().focusNode.parentElement);
    var oldHtml = parent.html();
    var newHtml = oldHtml.replace(date_str, "<span class='highlight' style='color:green'>"+formated_date+"</span>");
    parent.html( newHtml );

}

function addClickevents(){
    document.getElementById('disable-css').addEventListener('click', function() {
        disableCss()
    }, false);
    document.getElementById('zapper').addEventListener('click', function() {
        ZapPer()
    }, false);
    document.getElementById('modify-content').addEventListener('click', function() {
        modifyContent()
    }, false);
    document.getElementById('highlighter-btn').addEventListener('click', function() {
        annoHighlight()
    }, false);
    document.getElementById('phonetics-btn').addEventListener('click', function(event) {
        phoneticsTrans()
    }, false);
    document.getElementById('trans-text').addEventListener('click', function() {
        langTrans()
    }, false);
    document.getElementById('change-theme').addEventListener('click', function() {
        alternateStylesheets()
    }, false);
    document.getElementById('change-content').addEventListener('click', function() {
        showContent()
    }, false);
    document.getElementById('change-font').addEventListener('click', function() {
        changeFont()
    }, false);
    document.getElementById('change-currency').addEventListener('click', function() {
        currencyTrans()
    }, false);
    document.getElementById('change-measurement').addEventListener('click', function() {
        convertUnits()
    }, false);
    document.getElementById('change-num-sys').addEventListener('click', function() {
        convertNumsys()
    }, false);
    document.getElementById('change-date-format').addEventListener('click', function() {
        formatDate()
    }, false);
}


#+title:Annolet! Web Annotation Tool
#+AUTHOR: Sadhana V
#+DATE: [2017-06-17 Fri]
#+PROPERTY: results output
#+PROPERTY: exports code
#+SETUPFILE: ../org-templates/level-1.org
#+options: ^:nil
#+LATEX: Literal LaTeX code for export

* Function =annoletContainer()= 
This function creates a container div which, lets the users to
interact with app.  When annoletContainer() is called, it injects CSS
stylesheet into <head> element of webpage to stylize the container
and *<div id=annolet-container>--</div>* whose inner HTML is a list of
options inside body.  Here the css stylesheet is served from [[https://raw.githack.com/][GitHack]].
#+BEGIN_SRC js :tangle ../../code/annolet/annolet.js :eval no

// function to create container for annolet
function annoletContainer(){
    //appending a div(annolet container) to body element of a webpage.
    var body = document.getElementsByTagName('body')[0];
    container = document.createElement('div');
    container.id = 'annolet-container';
    body.appendChild(container);

    //appending a CSS stylesheet into head element of a webpage, which is used to stylize the annolet container.
    var linktag = document.createElement('link');
    linktag.rel = "stylesheet";
    linktag.type = "text/css";
    linktag.href = "https://gl.githack.com/renarration/web-annotation/raw/develop/src/annolet-bookmarklet/annolet/css/annolet.css"; 
    document.getElementsByTagName('head')[0].appendChild(linktag);

    //injecting html code
    container.innerHTML = "<h4 id='annolet-header'>Renarration</h4>"+
    "<ul id='annolet-tools-menu' >"+
        "<li class='annolet-menu-item' style='position:relative;top:-25px'>"+
            "<button id='disable-css' class='annolet-menu-sub-item' >No CSS</button>"+
        "</li>"+
        "<li class='annolet-menu-item' style='position:relative;top:-25px'>"+
            "<button id='zapper' class='annolet-menu-sub-item' >Zapper</button>"+
        "</li>"+
        "<li class='annolet-menu-item' style='position:relative;top:-25px'>"+
            "<button id='modify-content' class='annolet-menu-sub-item' >Modify Content</button>"+
        "</li>"+
        "<li class='annolet-menu-item' style='position:relative;top:-25px'>"+
            "<button id='highlighter-btn' class='annolet-menu-sub-item' >Highlighter</button>"+
        "</li>"+
        "<li class='annolet-menu-item' style='position:relative;top:-25px'>"+
            "<button id='phonetics-btn' class='annolet-menu-sub-item' >Phonetics</button>"+
        "</li>"+
        "<li class='annolet-menu-item'>"+
            "<button id='trans-text' class='annolet-menu-sub-item' >Translate Text</button>"+"<br>"+
            "<select class='select-tools-menu' id='select-from-lang'>"+
                "<option value='en' >English</option>"+
                "<option value='hi' >Hindi</option>"+
                "<option value='te' >Telugu</option>"+
                "<option value='ta' >Tamil</option>"+
                "<option value='ml' >Malayalam</option>"+
                "<option value='ja' >Japanese</option>"+
                "<option value='zh' >Chinese</option>"+
            "</select>"+
            "<select class='select-tools-menu' id='select-to-lang'>"+
                "<option value='zh' >Chinese</option>"+
                "<option value='ja' >Japanese</option>"+
                "<option value='ml' >Malayalam</option>"+
                "<option value='ta' >Tamil</option>"+
                "<option value='te' >Telugu</option>"+
                "<option value='hi' >Hindi</option>"+
                "<option value='en' >English</option>"+
            "</select>"+
        "</li>"+
        "<li class='annolet-menu-item'>"+
            "<button id='change-theme' class='annolet-menu-sub-item'>Switch CSS</button>"+"<br>"+
            "<select class='select-tools-menu' id='select-theme'>"+
                "<option value='switch1' >Theme1</option>"+
                "<option value='switch2' >Theme2</option>"+
                "<option value='switch3' >Theme3</option>"+
            "</select>"+
        "</li>"+
        "<li class='annolet-menu-item'>"+
            "<button id='change-content' class='annolet-menu-sub-item' >Page Stripper</button>"+"<br>"+
            "<select class='select-tools-menu' id='select-content'>"+
                "<option value='show-links' >Show Links</option>"+
                "<option value='show-text' >Show Text</option>"+
                "<option value='show-images' >Show Images</option>"+
            "</select>"+
        "</li>"+
        "<li class='annolet-menu-item'>"+
            "<button id='change-font' class='annolet-menu-sub-item' >Visibility</button>"+"<br>"+
            "<select class='select-tools-menu' id='select-font'>"+
                "<option value='increase-font' >Increase Font</option>"+
                "<option value='decrease-font' >Decrease Font</option>"+
            "</select>"+
        "</li>"+
        "<li class='annolet-menu-item'>"+
            "<button id='change-currency' class='annolet-menu-sub-item' >Convert Currency</button>"+"<br>"+
            "<select class='select-tools-menu' id='select-from-currency'>"+
                "<option value='USD' >USD</option>"+
                "<option value='INR' >INR</option>"+
                "<option value='EUR' >EUR</option>"+
            "</select>"+
            "<select class='select-tools-menu' id='select-to-currency'>"+
                "<option value='INR' >INR</option>"+
                "<option value='USD' >USD</option>"+
                "<option value='EUR' >EUR</option>"+
            "</select>"+
        "</li>"+
        "<li class='annolet-menu-item'>"+
            "<button id='change-measurement' class='annolet-menu-sub-item' >Convert Measurements</button>"+"<br>"+
            "<select class='select-tools-menu' id='select-from-measure'>"+
                "<option value='km'>kilometers</option>"+
                "<option value='mi'>Miles</option>"+
                "<option value='ft'>foot</option>"+
            "</select>"+
            "<select class='select-tools-menu' id='select-to-measure'>"+
                "<option value='mi'>Miles</option>"+
                "<option value='km'>kilometers</option>"+
                "<option value='ft'>foot</option>"+
            "</select>"+
        "</li>"+
    	"<li class='annolet-menu-item'>"+
            "<button id='change-num-sys' class='annolet-menu-sub-item' >Convert Num sys</button>"+"<br>"+
            "<select class='select-tools-menu' id='select-num-sys'>"+
                "<option value='en-IN' >Indian</option>"+
                "<option value='en-US' >US</option>"+
    	        "<option value='en-GB'>British</option>"+
    	        "<option value='ko-KR'>Korean</option>"+
    	        "<option value='ar-EG'>Arabic</option>"+
            "</select>"+
    	"</li>"+
        "<li class='annolet-menu-item'>"+
            "<button id='change-date-format' class='annolet-menu-sub-item' >Date Format</button>"+"<br>"+
            "<select class='select-tools-menu' id='select-date-format'>"+
                "<option value='en-IN' >Indian</option>"+
                "<option value='en-US' >US</option>"+
                "<option value='en-GB'>British</option>"+
                "<option value='ko-KR'>Korean</option>"+
                "<option value='ar-EG'>Arabic</option>"+
            "</select>"+
        "</li>"+
    "</ul>";
}

#+END_SRC
  

* Function =disableAllLinks()= 
This disableAllLinks() is to disable all links. When this is called it
iterates through all the links( =<a>= tags) in a webpage and disables
them, so that user will not go out of webpage while annotating the
webpage.
#+BEGIN_SRC js :tangle ../../code/annolet/annolet.js :eval no

// function to disable all links
function disableAllLinks(){
    var anchors = document.getElementsByTagName("a");
    for (var i = 0; i < anchors.length; i++) {
        anchors[i].onclick = function() {return(false);};
    }
}

#+END_SRC


* Function =disableCss()= 
This disableCss() is to disable the webpage css. When this function is
called it iterates through all the stylesheets ( =<link>= tags) in a
document and disables them and only one stylsheet which is used to
stylize the annolet container is made enabled.
#+BEGIN_SRC js :tangle ../../code/annolet/annolet.js :eval no

// function to remove the css
function disableCss(){
    var styleSheets = document.styleSheets;
    for ( var i=0; i<styleSheets.length; i++) {
        if(styleSheets[i].href == "https://gl.githack.com/renarration/web-annotation/raw/develop/src/annolet-bookmarklet/annolet/css/annolet.css"){
           styleSheets[i].disabled = false;
        }
        else{
            styleSheets[i].disabled = true;
        }
    }
}

#+END_SRC


* Function =ZapPer()= 
This ZapPer() allows the user to view their region of interest in a
webpage by allowing the user to remove the content which they doesn't
want to view by a click.
#+BEGIN_SRC js :tangle ../../code/annolet/annolet.js :eval no

//function to erase the content 
function  ZapPer(){
    $("body").click(function(event){
        var targetElem= event.target;
        if(targetElem.id == "annolet-container"||targetElem.id =="zapper"||targetElem.id =="annolet-header")
        {           
            targetElem.style.visibility = "visible";
        }
        else
        {
            targetElem.style.visibility = "hidden";
        }
    });
}

#+END_SRC


* Function =modifyContent()= 
This modifyContent() allows the users to modify content on a webpage.
#+BEGIN_SRC js :tangle ../../code/annolet/annolet.js :eval no

function modifyContent() {
    var all = document.getElementsByTagName("*");
    for (var i=0,  max=all.length; i < max; i++) { 
        all[i].setAttribute('contenteditable', true);
        all[i].setAttribute('title', 'Edit Content');
    }
}

#+END_SRC


* Function =annoHighlight()= 
This annoHighlight() checks whether the text has been selected or not?
if selected, then the string is passed to the highlightContent() which
highlights the text.
#+BEGIN_SRC js :tangle ../../code/annolet/annolet.js :eval no

function annoHighlight() {
    // method to select the text in firefox
    if(window.getSelection){
        var userSelection = window.getSelection()
        var userSelection_text = userSelection.toString();
        span_elem = document.createElement('span');
        span_elem.textContent = userSelection_text;
        highlightContent(span_elem, userSelection)
     }
     // method to select the text in chrome
     else if (document.selection && document.selection.type != "Control") {
         var userSelection = document.selection.createRange().text;
         span_elem = document.createElement('span');
         span_elem.textContent = userSelection;
         highlightContent(span_elem, userSelection)
     }     
}

#+END_SRC

  
* Function =highlightContent()= 
The highlightContent() highlights the text passed to it.
#+BEGIN_SRC js :tangle ../../code/annolet/annolet.js :eval no

function highlightContent(elem, str) {      
     elem.style.backgroundColor = "yellow";
     elem.id = 'highlighted'
     var range = str.getRangeAt(0);
     range.deleteContents();
     range.insertNode(elem);
}

#+END_SRC


* Function =annoPhonetic()=
This annoPhonetic() checks whether the text has been selected or not?
if selected, then the string is passed to the phoneticsTrans() which
translates the string to the phonetics.
#+BEGIN_SRC js :tangle ../../code/annolet/annolet.js :eval no

//function for getting phonetic
function annoPhonetic() {
   if (window.getSelection) 
   {
      var text_to_translate = window.getSelection().toString();
      phoneticsTrans(text_to_translate)
   } 
   else if (document.selection && document.selection.type != "Control") {
      var text_to_translate = document.selection.createRange().text;
      phoneticsTrans(text_to_translate)
   } 
}

#+END_SRC


* Function =phoneticsTrans()= 
This makes a request to the webservices and gets the phonetic
translation of the string that is passed to it.  Make sure that the
webservice is running.
#+BEGIN_SRC js :tangle ../../code/annolet/annolet.js :eval no

function phoneticsTrans(str){
    var url = "//localhost:5000/phonetic-trans"
    var xhr = new XMLHttpRequest();
    xhr.open("POST",url,true);
    xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
    xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
    xhr.send(JSON.stringify({"sentence":str}));
    xhr.onreadystatechange = function() {
        if (this.readyState==4 && this.status==200) {
            var res = this.responseText;
            var parent = $(window.getSelection().focusNode.parentElement);
            var oldHtml = parent.html();
            var newHtml = oldHtml.replace(str, "<span class='highlight' style='color:green'>"+res+"</span>");
            parent.html( newHtml );
        }
    }
}

#+END_SRC


* Function =annoLanguage()=
This annoLanguage() checks whether the text has been selected or not?
if selected, then the string is passed to the langTrans() which
translates the string to the user preferred language. 
#+BEGIN_SRC js :tangle ../../code/annolet/annolet.js :eval no

//function for getting language translation
function annoLanguage() {
   var from_lang = document.getElementById("select-from-lang").value;
   var to_lang = document.getElementById("select-to-lang").value;
   if (window.getSelection){
      var text_to_translate = window.getSelection().toString();
      langTrans(text_to_translate, from_lang, to_lang)
   } 
   else if (document.selection && document.selection.type != "Control") {
      var text_to_translate = document.selection.createRange().text;
      langTrans(text_to_translate, from_lang, to_lang)
   }  
}

#+END_SRC


* Function =langTrans()= 
This makes a request to the webservices and gets the language
translation of the string that is passed to it.  Make sure that the
webservice is running.
#+BEGIN_SRC js :tangle ../../code/annolet/annolet.js :eval no

function langTrans(str, from, to){
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "//localhost:5000/language-translive", true); 
    xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
    xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
    xhr.send(JSON.stringify({"sentence":str,"from-language":from,"to-language":to}));
    xhr.onreadystatechange = processRequest;
    function processRequest(e)
    {
        if (xhr.readyState == 4)
        {
            var res = xhr.responseText;
            var parent = $(window.getSelection().focusNode.parentElement);
            var oldHtml = parent.html();
            var newHtml = oldHtml.replace(str, "<span class='highlight' style='color:green'>"+res+"</span>");
            parent.html( newHtml );
        }
    }
}

#+END_SRC


* Function =alternateStylesheets()= 
This is called to create alternate stylesheets, which will be used to
change the theme of a web page.
#+BEGIN_SRC js :tangle ../../code/annolet/annolet.js :eval no

function alternateStylesheets(){
    //appending a CSS alternate stylesheets to head element of a webpage.
    var i= 0;
    var style_sheets = 3; 
    var css_themes =["https://gl.githack.com/renarration/web-annotation/raw/develop/src/annolet-bookmarklet/annolet/css/switch1.css", 
    "https://gl.githack.com/renarration/web-annotation/raw/develop/src/annolet-bookmarklet/annolet/css/switch2.css", 
    "https://gl.githack.com/renarration/web-annotation/raw/develop/src/annolet-bookmarklet/annolet/css/switch3.css"];
    var link_title =['switch1', 'switch2', 'switch3'];

    for(i=0; i<style_sheets; i++){
        var linktag = document.createElement('link');
        linktag.rel  = 'alternate stylesheet';
        linktag.type = 'text/css';
        linktag.href = css_themes[i];
        linktag.title= link_title[i];
        head  = document.getElementsByTagName('head')[0];
        head.appendChild(linktag);
    }
    var selected_theme = document.getElementById("select-theme").value;
    switchStyle(selected_theme)
}

#+END_SRC

  
* Function =switchStyle(css_title)= 
This is called to switch the theme of a web page.
#+BEGIN_SRC js :tangle ../../code/annolet/annolet.js :eval no

function switchStyle(css_title)
{   
   var i;
   var linktag = document.getElementsByTagName("link");
   for (i = 0; i < linktag.length; i++ ) {
        if ((linktag[i].rel.indexOf( "stylesheet" ) != -1) &&linktag[i].title) {
            linktag[i].disabled = true ;
            if (linktag[i].title == css_title) {
                linktag[i].disabled = false ;
            }
        }
   }
}

#+END_SRC


* Function =showContent()= 
This showContent() displays the selected content on the web page and
rest of the content is made hidden. For example if the user selected
content is text then only that text is shown and the rest of the
content like (links,images etc., are made hidden).
#+BEGIN_SRC js :tangle ../../code/annolet/annolet.js :eval no

function showContent(){
    var all = document.getElementsByTagName("*");
    var selected_content = document.getElementById("select-content").value;
    if(selected_content == 'show-links'){
        for (var i=0, max=all.length; i < max; i++) {
            var href_attribute = all[i].hasAttribute("href");
            var src_attribute = all[i].hasAttribute("src");
            if(href_attribute == true || src_attribute == true){
                all[i].style.visibility = 'visible';
            }
            else if(href_attribute == false && src_attribute == false){
                all[i].style.visibility = 'hidden';
            }
        }
    }
    else if(selected_content == 'show-text') {
        for (var i=0, max=all.length; i < max; i++) {
            if(all[i].innerHTML){
                all[i].style.visibility = 'visible';
            }
            else{
                all[i].style.visibility = 'hidden';
            }
        }
    }
    else if(selected_content == 'show-images') {
        for (var i=0, max=all.length; i < max; i++) {
            var src_attribute = all[i].hasAttribute("src");
            if(src_attribute == false){
                all[i].style.visibility = 'hidden';
            }
            else if(src_attribute == true){
                all[i].style.visibility = 'visible';
            }
        }
    }

    //get the menu bar id 
    document.getElementById('annolet-container').style.visibility='visible';
    var annolet_elems = document.getElementById('annolet-container').children;
    //This will make all children elements of div visible. 
    for(var i = 0; i < annolet_elems.length; i++){
        annolet_elems[i].style.visibility = 'visible';
    }
}


#+END_SRC


* Function =changeFont()= 
This changeFont() increases/decreases the font size of the webpage
content.
#+BEGIN_SRC js :tangle ../../code/annolet/annolet.js :eval no

function changeFont(){
    var fontSize = parseInt($('body').css('font-size'),10);
    var selected_font = document.getElementById("select-font").value;
    if(selected_font == 'increase-font'){
        fontSize +=1.5;
        $('body').css('font-size', fontSize+'px');
    }
    else if(selected_font == 'decrease-font'){
        fontSize -=1;
        $('body').css('font-size', fontSize+'px');
    }
}

#+END_SRC


* Function =annoCurrency()=
This annoCurrency() checks whether the currency has been selected or not?
if selected, then the currency is passed to the currencyTrans() which
translates the currency to the user preferred choice.  
#+BEGIN_SRC js :tangle ../../code/annolet/annolet.js :eval no

//function for getting currency translation
function annoCurrency() {
    var from_cur = document.getElementById('select-from-currency').value;
    var to_cur = document.getElementById('select-to-currency').value;
    if (window.getSelection) 
    {   
        var curncy = window.getSelection().toString();
        var remove_splchar =  curncy.replace(/\,/g,"");
        var cur_to_translate =  parseFloat(remove_splchar);
        currencyTrans(curncy, cur_to_translate, from_cur, to_cur)
    } 
    else if (document.selection && document.selection.type != "Control") {
        var curncy = document.selection.createRange().text;
        var remove_splchar =  curncy.replace(/\,/g,"");
        var cur_to_translate =  parseFloat(remove_splchar);
        currencyTrans(curncy, cur_to_translate, from_cur, to_cur)
    }
}

#+END_SRC


* Function =currencyTrans()= 
This makes a request to the webservices and gets the currency
conversion of the rupees that is passed to it. Make sure that the
webservice is running.
#+BEGIN_SRC js :tangle ../../code/annolet/annolet.js :eval no
 
function currencyTrans(cur_str, cur, from, to){
    var xhr = new XMLHttpRequest();
    var url = "//localhost:5000/currency-conversion"
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
    xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
    xhr.send(JSON.stringify({ "currency_amount":cur, "from_cur":from, "to_cur":to }));
    xhr.onreadystatechange = processRequest;
    function processRequest(e)
    {
        if (xhr.readyState == 4)
        {
            var res = xhr.responseText;
            var parent = $(window.getSelection().focusNode.parentElement);
            var oldHtml = parent.html();
            var newHtml = oldHtml.replace(cur_str, "<span class='highlight' style='color:green'>"+res+"</span>");
            parent.html( newHtml );
        }
    }
}


#+END_SRC


* Function =annoUnits()= 
This annoUnits() checks whether the units has been selected or not?
if selected, then the units is passed to the convertUnits() which
translates the units to the user preferred choice.  
#+BEGIN_SRC js :tangle ../../code/annolet/annolet.js :eval no

//function for getting units of measurement conversion
function annoUnits() {
    var from_unit = document.getElementById('select-from-measure').value;
    var to_unit = document.getElementById('select-to-measure').value;
    if (window.getSelection) 
    {
        var units = window.getSelection().toString();
        var remove_splchar =  units.replace(/\,/g,"");
        var units_to_convert =  parseFloat(remove_splchar);
        convertUnits(units, units_to_convert, from_unit, to_unit)
    } 
    else if (document.selection && document.selection.type != "Control") {
        var units = document.selection.createRange().text;
        var remove_splchar =  units.replace(/\,/g,"");
        var units_to_convert =  parseFloat(remove_splchar);
        convertUnits(units, units_to_convert, from_unit, to_unit)
    }
}

#+END_SRC


* Function =convertUnits()= 
This makes a request to the webservices and gets the conversion of
units of measurements of the unit that is passed to it. Make sure that
the webservice is running.

#+BEGIN_SRC js :tangle ../../code/annolet/annolet.js :eval no

function convertUnits(units_str, unt, from, to) {
    var xhr = new XMLHttpRequest();
    var url = "//localhost:5000/measurement-conversion";
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
    xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
    xhr.send(JSON.stringify({ "measurement_num": unt, "from_measure": from, "to_measure": to }));
    xhr.onreadystatechange = processRequest;
    function processRequest(e)
    {
        if (xhr.readyState == 4)
        {
            var res = xhr.responseText;
            var parent = $(window.getSelection().focusNode.parentElement);
            var oldHtml = parent.html();
            var newHtml = oldHtml.replace(units_str, "<span class='highlight' style='color:green'>"+res+"</span>");
            parent.html( newHtml );
        }
    }
}

#+END_SRC


* Function =convertNumsys()= 
This convertNumsys() converts the user selected number on a webpage to
their preferred number system.
#+BEGIN_SRC js :tangle ../../code/annolet/annolet.js :eval no

function convertNumsys(){
    var selected_numsys = document.getElementById("select-num-sys").value;
    if (window.getSelection) 
    {
        var number = window.getSelection().toString();
        var remove_splchar =  number.replace(/\,/g,"");
        var num_to_translate = parseInt(remove_splchar);
    } 
    else if (document.selection && document.selection.type != "Control") {
        var number = document.selection.createRange().text;
        var remove_splchar =  number.replace(/\,/g,"");
        var num_to_translate = parseInt(remove_splchar);
    }
    var translated_num = num_to_translate.toLocaleString(selected_numsys);
    var parent = $(window.getSelection().focusNode.parentElement);
    var oldHtml = parent.html();
    var newHtml = oldHtml.replace(number, "<span class='highlight' style='color:green'>"+translated_num+"</span>");
    parent.html( newHtml );
}

#+END_SRC


* Function =formatDate()= 
This formatDate() coverts the date selected by the user to the user
preferred country format.
#+BEGIN_SRC js :tangle ../../code/annolet/annolet.js :eval no

function formatDate(){
    var selected_format = document.getElementById("select-date-format").value;
    if (window.getSelection) 
    {
        var date_str = window.getSelection().toString();
        var date_to_format = new Date(date_str);
    } 
    else if (document.selection && document.selection.type != "Control") {
        var date_str = document.selection.createRange().text;
        var date_to_format = new Date(date_str);
    }
    var formated_date = date_to_format.toLocaleDateString(selected_format);
    var parent = $(window.getSelection().focusNode.parentElement);
    var oldHtml = parent.html();
    var newHtml = oldHtml.replace(date_str, "<span class='highlight' style='color:green'>"+formated_date+"</span>");
    parent.html( newHtml );

}

#+END_SRC


* Function =addClickevents()= 
On load of a webpage addClickevents() adds click events to the annolet
elements.

#+BEGIN_SRC js :tangle ../../code/annolet/annolet.js :eval no

function addClickevents(){
    document.getElementById('disable-css').addEventListener('click', function() {
        disableCss()
    }, false);
    document.getElementById('zapper').addEventListener('click', function() {
        ZapPer()
    }, false);
    document.getElementById('modify-content').addEventListener('click', function() {
        modifyContent()
    }, false);
    document.getElementById('highlighter-btn').addEventListener('click', function() {
        annoHighlight()
    }, false);
    document.getElementById('phonetics-btn').addEventListener('click', function(event) {
        annoPhonetic()
    }, false);
    document.getElementById('trans-text').addEventListener('click', function() {
        annoLanguage()
    }, false);
    document.getElementById('change-theme').addEventListener('click', function() {
        alternateStylesheets()
    }, false);
    document.getElementById('change-content').addEventListener('click', function() {
        showContent()
    }, false);
    document.getElementById('change-font').addEventListener('click', function() {
        changeFont()
    }, false);
    document.getElementById('change-currency').addEventListener('click', function() {
        annoCurrency()
    }, false);
    document.getElementById('change-measurement').addEventListener('click', function() {
        annoUnits()
    }, false);
    document.getElementById('change-num-sys').addEventListener('click', function() {
        convertNumsys()
    }, false);
    document.getElementById('change-date-format').addEventListener('click', function() {
        formatDate()
    }, false);
}

#+END_SRC


* Function to invoke the following functions when the page loads.

#+BEGIN_SRC js :tangle ../../code/annolet/annolet.js :eval no

window.onload = function() {
   disableAllLinks() 
   annoletContainer() 
   addClickevents()
};

#+END_SRC

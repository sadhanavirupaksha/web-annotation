# Bookmarklet for Renarration
## Setup Instructions

Bookmarklet URL: https://gl.githack.com/renarration/web-annotation/raw/master/src/annolet-bookmarklet/annolet/annolet.html

Following are the steps for injecting the annolet into a webpage.

Step1:   Please open the bookmarklet url in firefox.

Step2:   Drag the  "annotate" button onto the browser bookmark bar.

Step3:   To annotate go to any webpage and click on the Bookmarklet.


- For Lang translation, phonetics translation, currency translation, measurement translation on a web page you require to run webservice server.

Clone the following repository:
- `https://gitlab.com/renarration/web-annotation.git`

Go to the directory:

 - `cd src/annolet-bookmarklet/webservices`


Run setup.sh file to install the dependencies

- `bash setup.sh`

Activate virtual environment 
- `source ./flask/bin/activate`


Run the webservice 
- `python2.7 service.py`
